import React, { useState } from 'react'
import { Nav, Container, Navbar, Modal, Row, Col } from 'react-bootstrap'

function Header({current, setCurrent}) {
  const [show, setShow] = useState(true)
  const [city, setCity] = useState('Москва')

  const handleCity = (coor) => {
    setCurrent(coor)
    if(coor[0] == '55.755864' && coor[1] == '37.617698'){
      setCity('Москва')
    }
    if(coor[0] == '45.035470' && coor[1] == '38.975313'){
      setCity('Краснодар')
    }
    setShow(false)
  }
  
    return (
      <>
        <Navbar bg="white" expand="lg" style={{height: 64, boxShadow: '0 2px 4px 0 rgb(80 89 100 / 16%)'}}>
          <Container className='header-wrapper'>
            <Navbar.Brand href="#">
                <img className='logo' src='https://dostavista.ru/img/ru/logo.svg' />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
              <Nav
                className="me-auto my-2 my-lg-0"
                style={{ maxHeight: '100px' }}
                navbarScroll
              >
                <Nav.Link href="#" className="region-link d-flex align-items-center" onClick={() => setShow(true)}>
                    <svg width='20' height='20' viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg" fill="#2d2928" focusable="false">
                        <path fillRule="evenodd" clipRule="evenodd" d="M9 9.75a2.628 2.628 0 01-2.625-2.625A2.628 2.628 0 019 4.5a2.628 2.628 0 012.625 2.625A2.628 2.628 0 019 9.75zM9 1.5c-3.308 0-6 2.665-6 5.942 0 4.106 5.287 8.684 5.512 8.877a.748.748 0 00.976 0C9.713 16.127 15 11.548 15 7.442 15 4.165 12.308 1.5 9 1.5z"></path>
                    </svg>
                    <p className='region mb-0'>{city}</p>
                </Nav.Link>
              </Nav>
              <div className='d-flex align-items-center gap-4'>
                <Nav.Link href="https://couriers.dostavista.ru/">Стать курьером</Nav.Link>
                <Nav.Link href="#">Стать клиентом</Nav.Link>
                <button className='btn-outlined'>Вход/Регистрация</button>
              </div>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <Modal show={show} onHide={() => setShow(false)}>
          <Modal.Header closeButton>
            <Modal.Title style={{ fontWeight: 700, padding: 8, paddingBottom: 0 }}>Ваш регион доставки</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ padding: 24 }}>
            <Row>
              <Col xs={4} style={{ fontWeight: current[0] == '55.755864' ? 700 : 400 }} onClick={() => handleCity([55.755864, 37.617698])} className='city mb-3'>Москва</Col>
              <Col xs={4} style={{ fontWeight: current[0] == '45.035470' ? 700 : 400 }} onClick={() => handleCity([45.035470, 38.975313])} className='city mb-3'>Краснодар</Col>
              <Col xs={4} style={{ fontWeight: current === '6' ? 700 : 400 }} onClick={() => setCurrent('6')} className='city mb-3'>Санкт-Петербург</Col>
              <Col xs={4} style={{ fontWeight: current === '7' ? 700 : 400 }} onClick={() => setCurrent('7')} className='city mb-3'>Екатеринбург</Col>
              <Col xs={4} style={{ fontWeight: current === '8' ? 700 : 400 }} onClick={() => setCurrent('8')} className='city mb-3'>Новосибирск</Col>
              <Col xs={4} style={{ fontWeight: current === '9' ? 700 : 400 }} onClick={() => setCurrent('9')} className='city mb-3'>Казань</Col>
              <Col xs={4} style={{ fontWeight: current === '10' ? 700 : 400 }} onClick={() => setCurrent('10')} className='city mb-3'>Нижний Новгород</Col>
              <Col xs={4} style={{ fontWeight: current === '1' ? 700 : 400 }} onClick={() => setCurrent('1')} className='city mb-3'>Челябинск</Col>
              <Col xs={4} style={{ fontWeight: current === '2' ? 700 : 400 }} onClick={() => setCurrent('2')} className='city mb-3'>Уфа</Col>
              <Col xs={4} style={{ fontWeight: current === '3' ? 700 : 400 }} onClick={() => setCurrent('3')} className='city'>Пермь</Col>
              <Col xs={4} style={{ fontWeight: current === '4' ? 700 : 400 }} onClick={() => setCurrent('4')} className='city'>Саратов</Col>
              <Col xs={4} style={{ fontWeight: current === '5' ? 700 : 400 }} onClick={() => setCurrent('5')} className='city'>Барнаул</Col>
            </Row>
          </Modal.Body>
        </Modal>
      </>
    );
}

export default Header