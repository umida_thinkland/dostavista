import React, { useState, useRef, useEffect, useMemo } from 'react';
import Header from './components/Header'
import { Card, Form, InputGroup, Image, Row, Col, Modal } from 'react-bootstrap'
import { YMaps, Map, Placemark } from 'react-yandex-maps'
import { MaskedInput, Input, SelectPicker, Popover, Whisper } from 'rsuite'
import 'rsuite/dist/rsuite.min.css';

function App() {
  const [currentCity, setCurrentCity] = useState([55.755864, 37.617698])
  const [more, setMore] = useState(false)
  const [selectCard, setSelectCard] = useState('first')
  const [selectTransport, setSelectTransport] = useState('on-foot')
  const [selectWeight, setSelectWeight] = useState(1)
  const [selectCar, setSelectCar] = useState('jip')
  const [orderType, setOrderType] = useState('')
  const focusInput = useRef(null)
  const totalCard = useRef(null)
  const [packageValue, setPackageValue] = useState('')
  const [paymentMethod, setPaymentMethod] = useState('cash')
  const [promokod, setPromokod] = useState(false)
  const [show, setShow] = useState(false)
  const [value, setValue] = useState('')  
  const [more2, setMore2] = useState('')   
  const [couirer, setCouirer] = useState(false) 
  const [summa, setSumma] = useState(120)  
  const [height, setHeight] = useState(true)
  const [destination, setDestination] = useState([{more: false, pointStyle: {display: 'none'}, btnStyle: {display: 'none'}, cardStyle: {paddingTop: 16}}])  
  const phoneValidation = {
    mask: [ '+', 9, 9, 8, ' ', /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/],
    placeholder: '+998 90 986 35 85',
  }
  const data = ['Eugenia', 'Bryan', 'Linda', 'Nancy', 'Lloyd', 'Alice', 'Julia', 'Albert'].map(
    item => ({ label: item, value: item })
  )
  
  useEffect(() => {
    window.addEventListener('scroll', itogo)
    return function cleanup() {
      document.removeEventListener('scroll', itogo)
    }
  }, [window.scrollY])

  const itogo = () => {
    if(document.querySelector('.total-card')){
      if(document.querySelector('.total-card').getBoundingClientRect().y<620){
        setHeight(false)
      } else {
        setHeight(true)
      }
    }
  }

  const mapState = useMemo(
    () => ({ center: currentCity, zoom: 12 }),
    [currentCity]
  )

  const handleChangeFoot = () => {
    setSelectTransport('on-foot')
    setSelectWeight(1)
  }

  const handleChangeCar = () => {
    setSelectTransport('car')
    setSelectWeight(50)
  }

  const handleOther = () => {
    setOrderType('')
    focusInput.current.focus()
  }

  const handlePackageValue = (e) => {
    if(e.target.value > 50000){
      setPackageValue(50000)
    } else {
      setPackageValue(e.target.value)
    }
  }

  const handleAddDestination = () => {
    setDestination(state => {
      const newState = state.map(d => ({ ...d, pointStyle: {display: 'inherit', top: 34 }, btnStyle: {display: 'inherit'}, cardStyle: {paddingTop: 36}}))
      return [...newState, {more: false, pointStyle: {display: 'none'}, btnStyle: {display: 'inherit'}, cardStyle: {paddingTop: 36}}]
    })
  }

  const handleRemoveDestination = (index) => {
    setDestination(state => {
      if(state.length === 2){
        return state.filter((n, i) => i !== index).map(n => ({...n, pointStyle: {display: 'none'}, btnStyle: {display: 'none'}, cardStyle: {paddingTop: 16}}))
      } else return state.filter((n, i) => i !== index)
    })
  }

  const handleMore = (index) => {
    setDestination(state => state.map((d, i) => {
      if(i === index){
        return {...d, more: true}
      } else return d
    }))
  }

  const handleCloseMore = (index) => {
    setDestination(state => state.map((d, i) => {
      if(i === index){
        return {...d, more: false}
      } else return d
    }))
  }

  const handleModal = () => {
    setShow(false)
    setCouirer(true)
  }

  useEffect(()=>{
    if(selectTransport === 'on-foot'){
      setSumma(120)
    } else if(selectTransport === 'car'){
      setSumma(150)
    } else setSumma(1350)
    setSumma(s => Number((packageValue*0.005).toFixed(0)) + s)
  }, [packageValue, selectTransport])


  return (
    <div className="App">
      <Header current={currentCity} setCurrent={setCurrentCity} />
      <div className='wrapper'>
        <div className='container-custom'>
          <h1 className='header ml-144 mr-24 '>Заказ</h1>
          <Card className='mt-5 ml-144 mr-24 shadow-sm'>
            <Card.Body>
              <div className='d-flex gap-2'>
                <div className='box' style={{ borderColor: selectCard === 'first' ? '#ff3d81' : '#ddd' }} onClick={() => setSelectCard('first')}>
                  <svg className={selectCard === 'first' ? 'main-svg active' : 'main-svg'} width="24" height="24" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M35.62 14h.24c14.627.16 26.298 12.071 26.138 26.62-.08 14.629-11.99 26.46-26.618 26.38C20.75 66.92 8.92 55.009 9 40.38 9.08 25.751 20.99 13.92 35.62 14zm-.014 3h.212c12.973.142 23.322 10.705 23.18 23.606C58.929 53.58 48.366 64.07 35.394 64 22.421 63.929 11.93 53.366 12 40.394 12.071 27.42 22.633 16.93 35.606 17z" fill="#969493"></path><path d="M35.5 23c0-1.657 1.35-3.023 2.99-2.78a20.503 20.503 0 0117.29 17.29c.242 1.64-1.123 2.99-2.78 2.99H38.5a3 3 0 01-3-3V23zM35.732 5h-.549C32.294 5 30 7.25 30 10.083v.75c0 .084.085.167.17.167h10.66c.085 0 .17-.083.17-.167v-.75C40.915 7.25 38.62 5 35.732 5z" fill="#969493"></path></svg>
                  <p className='py-2 fw-bold fs-6 mb-0'>Как можно скорее</p>
                  <p className='desc'>Заказ заберёт и доставит ближайший свободный курьер</p>
                  <p className='money'>от 120 ₽</p>
                  { more && 
                    <>
                      <hr />
                      <div className='d-flex gap-2'>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 5.373 5.373 0 12 0s12 5.373 12 12-5.373 12-12 12S0 18.627 0 12z" fill="#0048FF" fill-opacity="0.1"></path><path d="M13.278 11.516a4.434 4.434 0 002.617 1.354c.38.052.681-.263.681-.63 0-.375-.304-.66-.653-.727-.843-.162-1.533-.67-1.91-1.332h0l-.002-.002-.594-.95h0l-.001-.002c-.248-.373-.627-.629-1.075-.629-.096 0-.177.016-.25.03l-.002.001a1.079 1.079 0 01-.223.028h-.015l-.015.007-2.71 1.146a.697.697 0 00-.426.642v1.787a.672.672 0 001.344 0v-1.002a.54.54 0 01.345-.504l.542-.21-.802 4.06a.54.54 0 01-.638.424l-1.706-.348a.682.682 0 10-.27 1.338l2.973.595c.37.074.732-.16.816-.529l.43-1.89.955.91a.54.54 0 01.168.39v2.704a.672.672 0 101.344 0v-3.594a.697.697 0 00-.216-.505l-1.024-.975.317-1.587zm-.046-3.059a1.27 1.27 0 001.266-1.265 1.27 1.27 0 00-1.266-1.266 1.27 1.27 0 00-1.266 1.266c0 .696.57 1.265 1.266 1.265z" fill="#0048FF" stroke="#0048FF" stroke-width="0.156"></path></svg>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 5.373 5.373 0 12 0s12 5.373 12 12-5.373 12-12 12S0 18.627 0 12z" fill="#A62EFF" fill-opacity="0.1"></path><path fillRule="evenodd" clipRule="evenodd" d="M15.893 8.075a.841.841 0 00-.8-.575H8.907c-.371 0-.68.24-.799.575L6.992 11.33a1 1 0 00-.054.324v4.39c0 .313.253.57.562.57h.563c.309 0 .562-.257.562-.57v-.57h6.75v.57c0 .313.253.57.563.57h.562c.31 0 .563-.257.563-.57v-4.39a1 1 0 00-.055-.324l-1.115-3.254zm-7.83 2.842l.843-2.563h6.188l.844 2.563H8.061zm1.723 2.062a.922.922 0 11-1.845 0 .922.922 0 011.845 0zm5.35.922a.922.922 0 100-1.845.922.922 0 000 1.845z" fill="#A62EFF"></path><path d="M15.893 8.075l-.214.072v.001l.213-.073zm-7.786 0l-.212-.074v.001l.213.073zM6.992 11.33l.212.073-.212-.073zm1.633 4.144v-.225H8.4v.225h.225zm6.75 0h.225v-.225h-.225v.225zm1.633-4.144l-.212.073.212-.073zM8.906 8.354V8.13h-.163l-.05.155.213.07zm-.844 2.563l-.213-.07-.097.295h.31v-.225zm7.032-2.563l.213-.07-.05-.155h-.163v.225zm.844 2.563v.225h.31l-.097-.295-.213.07zm-.844-3.192c.272 0 .503.175.585.422l.427-.143a1.066 1.066 0 00-1.012-.729v.45zm-6.188 0h6.188v-.45H8.906v.45zm-.586.425a.622.622 0 01.586-.425v-.45c-.473 0-.863.306-1.01.726l.424.149zm-1.116 3.252L8.32 8.148l-.425-.146-1.116 3.254.425.146zm-.042.251c0-.085.015-.17.042-.251l-.425-.146a1.225 1.225 0 00-.066.397h.45zm0 4.39v-4.39h-.45v4.39h.45zm.338.345a.343.343 0 01-.338-.345h-.45c0 .435.352.794.788.794v-.45zm.563 0H7.5v.45h.563v-.45zm.337-.345a.343.343 0 01-.338.345v.45c.437 0 .788-.36.788-.795H8.4zm0-.57v.57h.45v-.57H8.4zm6.975-.225h-6.75v.45h6.75v-.45zm.225.795v-.57h-.45v.57h.45zm.338.345a.343.343 0 01-.338-.345h-.45c0 .435.351.794.787.794v-.45zm.562 0h-.563v.45h.563v-.45zm.337-.345a.343.343 0 01-.337.345v.45c.436 0 .788-.36.788-.795h-.45zm0-4.39v4.39h.45v-4.39h-.45zm-.041-.251c.027.08.041.166.041.251h.45c0-.135-.022-.27-.066-.397l-.425.146zM15.68 8.148l1.116 3.254.425-.146-1.116-3.254-.425.146zm-6.987.136l-.844 2.563.427.14.844-2.562-.427-.141zm6.4-.155H8.907v.45h6.188v-.45zm1.058 2.718l-.844-2.563-.427.14.844 2.564.427-.141zm-8.088.295h7.874v-.45H8.064v.45zm.8 2.984c.634 0 1.148-.513 1.148-1.147h-.45a.697.697 0 01-.697.697v.45zM7.717 12.98c0 .634.514 1.147 1.148 1.147v-.45a.697.697 0 01-.698-.697h-.45zm1.148-1.148c-.634 0-1.148.514-1.148 1.148h.45c0-.385.312-.698.698-.698v-.45zm1.147 1.148c0-.634-.514-1.148-1.147-1.148v.45c.385 0 .697.313.697.698h.45zm5.823 0a.697.697 0 01-.698.697v.45c.634 0 1.148-.513 1.148-1.147h-.45zm-.698-.698c.386 0 .698.313.698.698h.45c0-.634-.514-1.148-1.148-1.148v.45zm-.697.698c0-.385.312-.698.697-.698v-.45c-.633 0-1.147.514-1.147 1.148h.45zm.697.697a.697.697 0 01-.697-.697h-.45c0 .634.514 1.147 1.147 1.147v-.45z" fill="#A62EFF"></path></svg>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 5.373 5.373 0 12 0s12 5.373 12 12-5.373 12-12 12S0 18.627 0 12z" fill="#009FFF" fill-opacity="0.1"></path><path fillRule="evenodd" clipRule="evenodd" d="M6.32 6.73a1 1 0 00-1 1v7.251a1 1 0 001 1h.035a2.056 2.056 0 014.098 0h3.097a2.056 2.056 0 014.098 0h.034a1 1 0 001-1v-3.02a2 2 0 00-.336-1.11l-2.45-3.675a1 1 0 00-.832-.446H6.32zm6.71 1.334v3.166h3.983l-2.13-3.166h-1.854zm-3.317 8.09a1.294 1.294 0 11-2.588 0 1.294 1.294 0 012.588 0zm5.886 1.294a1.294 1.294 0 100-2.588 1.294 1.294 0 000 2.588z" fill="#009FFF"></path></svg>
                      </div>
                      <p className='more-text'>Доставим пешком, на авто или грузовике</p>
                      <div className='icon-container'>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.205 8.51C4.72 7.402 4.5 6.258 4.5 5.572c0-1.657 2.015-3 4.5-3s4.5 1.343 4.5 3c0 .686-.22 1.83-.705 2.938a4.5 4.5 0 11-7.59 0zm6.66-1.05c.267-.765.385-1.481.385-1.888 0-.28-.17-.687-.761-1.081-.583-.388-1.459-.669-2.489-.669-1.03 0-1.906.28-2.489.669-.59.394-.761.8-.761 1.081 0 .407.118 1.123.385 1.887A4.482 4.482 0 019 6.43c1.089 0 2.087.387 2.865 1.03z" fill="#969493"></path></svg>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.205 8.51C4.72 7.402 4.5 6.258 4.5 5.572c0-1.657 2.015-3 4.5-3s4.5 1.343 4.5 3c0 .686-.22 1.83-.705 2.938a4.5 4.5 0 11-7.59 0zm6.66-1.05c.267-.765.385-1.481.385-1.888 0-.28-.17-.687-.761-1.081-.583-.388-1.459-.669-2.489-.669-1.03 0-1.906.28-2.489.669-.59.394-.761.8-.761 1.081 0 .407.118 1.123.385 1.887A4.482 4.482 0 019 6.43c1.089 0 2.087.387 2.865 1.03z" fill="#969493"></path></svg>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.205 8.51C4.72 7.402 4.5 6.258 4.5 5.572c0-1.657 2.015-3 4.5-3s4.5 1.343 4.5 3c0 .686-.22 1.83-.705 2.938a4.5 4.5 0 11-7.59 0zm6.66-1.05c.267-.765.385-1.481.385-1.888 0-.28-.17-.687-.761-1.081-.583-.388-1.459-.669-2.489-.669-1.03 0-1.906.28-2.489.669-.59.394-.761.8-.761 1.081 0 .407.118 1.123.385 1.887A4.482 4.482 0 019 6.43c1.089 0 2.087.387 2.865 1.03z" fill="#969493"></path></svg>
                      </div>
                      <p className='more-text'>Возьмём до 1 500 кг или 17 куб. м. за раз</p>
                      <div style={{ width: 42 }} className='icon-container'>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M8.388 14.998a1.249 1.249 0 001.224 0l.19-.106a9.474 9.474 0 004.823-8.248v-.09c0-.45-.244-.867-.637-1.09L9.611 3.003a1.255 1.255 0 00-1.225 0L4.011 5.465c-.392.222-.636.64-.636 1.088v.091a9.474 9.474 0 004.824 8.248l.19.106zm1.06-10.381l3.198 1.799a.916.916 0 01.465.796v.066a6.925 6.925 0 01-3.525 6.029l-.138.077c-.225.127-.448-.093-.448-.351V4.967c0-.258.222-.477.447-.35z" fill="#969493"></path></svg>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M8.388 14.998a1.249 1.249 0 001.224 0l.19-.106a9.474 9.474 0 004.823-8.248v-.09c0-.45-.244-.867-.637-1.09L9.611 3.003a1.255 1.255 0 00-1.225 0L4.011 5.465c-.392.222-.636.64-.636 1.088v.091a9.474 9.474 0 004.824 8.248l.19.106zm1.06-10.381l3.198 1.799a.916.916 0 01.465.796v.066a6.925 6.925 0 01-3.525 6.029l-.138.077c-.225.127-.448-.093-.448-.351V4.967c0-.258.222-.477.447-.35z" fill="#969493"></path></svg>
                      </div>
                      <p className='more-text'>Вернём до 50000 ₽ при поломке или утере</p>
                      <div style={{ width: 24 }} className='icon-container'>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M13.52 12.435l-1.29-.58V5.39l1.29.58v6.465zM7.064 5.39l1.291.58v6.465l-1.291-.58V5.39zm7.369-.426l-2.584-1.162-.03-.009a.717.717 0 00-.317-.04c-.024.003-.046.01-.07.015a.788.788 0 00-.083.025c-.01.004-.02.005-.03.01L9 4.843 6.681 3.802l-.03-.009a.714.714 0 00-.316-.04c-.024.003-.047.01-.07.015a.784.784 0 00-.084.025c-.01.004-.02.005-.03.01L3.57 4.962a.645.645 0 00-.381.59v7.88a.647.647 0 00.91.589l2.319-1.042 2.318 1.042c.01.005.02.003.03.006a.63.63 0 00.235.05.63.63 0 00.234-.05c.01-.003.022-.002.03-.006l2.32-1.042 2.318 1.042a.645.645 0 00.91-.589v-7.88a.645.645 0 00-.38-.59z" fill="#969493"></path></svg>
                      </div>
                      <p className='more-text'>Работаем по городу и области</p>
                    </>
                  }
                </div>
                <div className='box' style={{ borderColor: selectCard === 'second' ? '#ff3d81' : '#ddd' }} onClick={() => setSelectCard('second')}>
                  <svg className={selectCard === 'second' ? 'main-svg active' : 'main-svg'} width="24" height="24" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.732 5h-.549C19.294 5 17 7.25 17 10.083v.75c0 .084.085.167.17.167h10.66c.085 0 .17-.083.17-.167v-.75C27.915 7.25 25.62 5 22.732 5zM31 25.8a1.8 1.8 0 011.8-1.8h5.4a1.8 1.8 0 011.8 1.8v5.4a1.8 1.8 0 01-1.8 1.8h-5.4a1.8 1.8 0 01-1.8-1.8v-5.4zM44.8 24a1.8 1.8 0 00-1.8 1.8v5.4a1.8 1.8 0 001.8 1.8h5.4a1.8 1.8 0 001.8-1.8v-5.4a1.8 1.8 0 00-1.8-1.8h-5.4zM19 37.8a1.8 1.8 0 011.8-1.8h5.4a1.8 1.8 0 011.8 1.8v5.4a1.8 1.8 0 01-1.8 1.8h-5.4a1.8 1.8 0 01-1.8-1.8v-5.4zM32.8 36a1.8 1.8 0 00-1.8 1.8v5.4a1.8 1.8 0 001.8 1.8h5.4a1.8 1.8 0 001.8-1.8v-5.4a1.8 1.8 0 00-1.8-1.8h-5.4zM43 37.8a1.8 1.8 0 011.8-1.8h5.4a1.8 1.8 0 011.8 1.8v5.4a1.8 1.8 0 01-1.8 1.8h-5.4a1.8 1.8 0 01-1.8-1.8v-5.4zM20.8 48a1.8 1.8 0 00-1.8 1.8v5.4a1.8 1.8 0 001.8 1.8h5.4a1.8 1.8 0 001.8-1.8v-5.4a1.8 1.8 0 00-1.8-1.8h-5.4zM31 49.8a1.8 1.8 0 011.8-1.8h5.4a1.8 1.8 0 011.8 1.8v5.4a1.8 1.8 0 01-1.8 1.8h-5.4a1.8 1.8 0 01-1.8-1.8v-5.4zM44.8 48a1.8 1.8 0 00-1.8 1.8v5.4a1.8 1.8 0 001.8 1.8h5.4a1.8 1.8 0 001.8-1.8v-5.4a1.8 1.8 0 00-1.8-1.8h-5.4z" fill="#969493"></path><path fillRule="evenodd" clipRule="evenodd" d="M55 14H16c-3.9 0-7 3.1-7 7v39c0 3.9 3.1 7 7 7h39c3.9 0 7-3.1 7-7V21c0-3.9-3.1-7-7-7zm-2.208 3H18.208A6.17 6.17 0 0012 23.207v34.585A6.17 6.17 0 0018.207 64h34.585A6.17 6.17 0 0059 57.792V23.208A6.17 6.17 0 0052.792 17z" fill="#969493"></path><path d="M48.183 5h.549c2.889 0 5.183 2.25 5.268 5.083v.75c0 .084-.085.167-.17.167H43.17c-.085 0-.17-.083-.17-.167v-.75C43 7.25 45.294 5 48.183 5z" fill="#969493"></path></svg>
                  <p className='py-2 fw-bold fs-6 mb-0'>Запланировать</p>
                  <p className='desc'>Курьер будет на адресах в удобное вам время</p>
                  <p className='money'>от 120 ₽</p>
                  { more && 
                    <>
                      <hr />
                      <div className='d-flex gap-2'>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 5.373 5.373 0 12 0s12 5.373 12 12-5.373 12-12 12S0 18.627 0 12z" fill="#0048FF" fill-opacity="0.1"></path><path d="M13.278 11.516a4.434 4.434 0 002.617 1.354c.38.052.681-.263.681-.63 0-.375-.304-.66-.653-.727-.843-.162-1.533-.67-1.91-1.332h0l-.002-.002-.594-.95h0l-.001-.002c-.248-.373-.627-.629-1.075-.629-.096 0-.177.016-.25.03l-.002.001a1.079 1.079 0 01-.223.028h-.015l-.015.007-2.71 1.146a.697.697 0 00-.426.642v1.787a.672.672 0 001.344 0v-1.002a.54.54 0 01.345-.504l.542-.21-.802 4.06a.54.54 0 01-.638.424l-1.706-.348a.682.682 0 10-.27 1.338l2.973.595c.37.074.732-.16.816-.529l.43-1.89.955.91a.54.54 0 01.168.39v2.704a.672.672 0 101.344 0v-3.594a.697.697 0 00-.216-.505l-1.024-.975.317-1.587zm-.046-3.059a1.27 1.27 0 001.266-1.265 1.27 1.27 0 00-1.266-1.266 1.27 1.27 0 00-1.266 1.266c0 .696.57 1.265 1.266 1.265z" fill="#0048FF" stroke="#0048FF" stroke-width="0.156"></path></svg>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 5.373 5.373 0 12 0s12 5.373 12 12-5.373 12-12 12S0 18.627 0 12z" fill="#A62EFF" fill-opacity="0.1"></path><path fillRule="evenodd" clipRule="evenodd" d="M15.893 8.075a.841.841 0 00-.8-.575H8.907c-.371 0-.68.24-.799.575L6.992 11.33a1 1 0 00-.054.324v4.39c0 .313.253.57.562.57h.563c.309 0 .562-.257.562-.57v-.57h6.75v.57c0 .313.253.57.563.57h.562c.31 0 .563-.257.563-.57v-4.39a1 1 0 00-.055-.324l-1.115-3.254zm-7.83 2.842l.843-2.563h6.188l.844 2.563H8.061zm1.723 2.062a.922.922 0 11-1.845 0 .922.922 0 011.845 0zm5.35.922a.922.922 0 100-1.845.922.922 0 000 1.845z" fill="#A62EFF"></path><path d="M15.893 8.075l-.214.072v.001l.213-.073zm-7.786 0l-.212-.074v.001l.213.073zM6.992 11.33l.212.073-.212-.073zm1.633 4.144v-.225H8.4v.225h.225zm6.75 0h.225v-.225h-.225v.225zm1.633-4.144l-.212.073.212-.073zM8.906 8.354V8.13h-.163l-.05.155.213.07zm-.844 2.563l-.213-.07-.097.295h.31v-.225zm7.032-2.563l.213-.07-.05-.155h-.163v.225zm.844 2.563v.225h.31l-.097-.295-.213.07zm-.844-3.192c.272 0 .503.175.585.422l.427-.143a1.066 1.066 0 00-1.012-.729v.45zm-6.188 0h6.188v-.45H8.906v.45zm-.586.425a.622.622 0 01.586-.425v-.45c-.473 0-.863.306-1.01.726l.424.149zm-1.116 3.252L8.32 8.148l-.425-.146-1.116 3.254.425.146zm-.042.251c0-.085.015-.17.042-.251l-.425-.146a1.225 1.225 0 00-.066.397h.45zm0 4.39v-4.39h-.45v4.39h.45zm.338.345a.343.343 0 01-.338-.345h-.45c0 .435.352.794.788.794v-.45zm.563 0H7.5v.45h.563v-.45zm.337-.345a.343.343 0 01-.338.345v.45c.437 0 .788-.36.788-.795H8.4zm0-.57v.57h.45v-.57H8.4zm6.975-.225h-6.75v.45h6.75v-.45zm.225.795v-.57h-.45v.57h.45zm.338.345a.343.343 0 01-.338-.345h-.45c0 .435.351.794.787.794v-.45zm.562 0h-.563v.45h.563v-.45zm.337-.345a.343.343 0 01-.337.345v.45c.436 0 .788-.36.788-.795h-.45zm0-4.39v4.39h.45v-4.39h-.45zm-.041-.251c.027.08.041.166.041.251h.45c0-.135-.022-.27-.066-.397l-.425.146zM15.68 8.148l1.116 3.254.425-.146-1.116-3.254-.425.146zm-6.987.136l-.844 2.563.427.14.844-2.562-.427-.141zm6.4-.155H8.907v.45h6.188v-.45zm1.058 2.718l-.844-2.563-.427.14.844 2.564.427-.141zm-8.088.295h7.874v-.45H8.064v.45zm.8 2.984c.634 0 1.148-.513 1.148-1.147h-.45a.697.697 0 01-.697.697v.45zM7.717 12.98c0 .634.514 1.147 1.148 1.147v-.45a.697.697 0 01-.698-.697h-.45zm1.148-1.148c-.634 0-1.148.514-1.148 1.148h.45c0-.385.312-.698.698-.698v-.45zm1.147 1.148c0-.634-.514-1.148-1.147-1.148v.45c.385 0 .697.313.697.698h.45zm5.823 0a.697.697 0 01-.698.697v.45c.634 0 1.148-.513 1.148-1.147h-.45zm-.698-.698c.386 0 .698.313.698.698h.45c0-.634-.514-1.148-1.148-1.148v.45zm-.697.698c0-.385.312-.698.697-.698v-.45c-.633 0-1.147.514-1.147 1.148h.45zm.697.697a.697.697 0 01-.697-.697h-.45c0 .634.514 1.147 1.147 1.147v-.45z" fill="#A62EFF"></path></svg>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 5.373 5.373 0 12 0s12 5.373 12 12-5.373 12-12 12S0 18.627 0 12z" fill="#009FFF" fill-opacity="0.1"></path><path fillRule="evenodd" clipRule="evenodd" d="M6.32 6.73a1 1 0 00-1 1v7.251a1 1 0 001 1h.035a2.056 2.056 0 014.098 0h3.097a2.056 2.056 0 014.098 0h.034a1 1 0 001-1v-3.02a2 2 0 00-.336-1.11l-2.45-3.675a1 1 0 00-.832-.446H6.32zm6.71 1.334v3.166h3.983l-2.13-3.166h-1.854zm-3.317 8.09a1.294 1.294 0 11-2.588 0 1.294 1.294 0 012.588 0zm5.886 1.294a1.294 1.294 0 100-2.588 1.294 1.294 0 000 2.588z" fill="#009FFF"></path></svg>
                      </div>
                      <p className='more-text'>Доставим пешком, на авто или грузовике</p>
                      <div className='icon-container'>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.205 8.51C4.72 7.402 4.5 6.258 4.5 5.572c0-1.657 2.015-3 4.5-3s4.5 1.343 4.5 3c0 .686-.22 1.83-.705 2.938a4.5 4.5 0 11-7.59 0zm6.66-1.05c.267-.765.385-1.481.385-1.888 0-.28-.17-.687-.761-1.081-.583-.388-1.459-.669-2.489-.669-1.03 0-1.906.28-2.489.669-.59.394-.761.8-.761 1.081 0 .407.118 1.123.385 1.887A4.482 4.482 0 019 6.43c1.089 0 2.087.387 2.865 1.03z" fill="#969493"></path></svg>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.205 8.51C4.72 7.402 4.5 6.258 4.5 5.572c0-1.657 2.015-3 4.5-3s4.5 1.343 4.5 3c0 .686-.22 1.83-.705 2.938a4.5 4.5 0 11-7.59 0zm6.66-1.05c.267-.765.385-1.481.385-1.888 0-.28-.17-.687-.761-1.081-.583-.388-1.459-.669-2.489-.669-1.03 0-1.906.28-2.489.669-.59.394-.761.8-.761 1.081 0 .407.118 1.123.385 1.887A4.482 4.482 0 019 6.43c1.089 0 2.087.387 2.865 1.03z" fill="#969493"></path></svg>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.205 8.51C4.72 7.402 4.5 6.258 4.5 5.572c0-1.657 2.015-3 4.5-3s4.5 1.343 4.5 3c0 .686-.22 1.83-.705 2.938a4.5 4.5 0 11-7.59 0zm6.66-1.05c.267-.765.385-1.481.385-1.888 0-.28-.17-.687-.761-1.081-.583-.388-1.459-.669-2.489-.669-1.03 0-1.906.28-2.489.669-.59.394-.761.8-.761 1.081 0 .407.118 1.123.385 1.887A4.482 4.482 0 019 6.43c1.089 0 2.087.387 2.865 1.03z" fill="#969493"></path></svg>
                      </div>
                      <p className='more-text'>Возьмём до 1 500 кг или 17 куб. м. за раз</p>
                      <div style={{ width: 42 }} className='icon-container'>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M8.388 14.998a1.249 1.249 0 001.224 0l.19-.106a9.474 9.474 0 004.823-8.248v-.09c0-.45-.244-.867-.637-1.09L9.611 3.003a1.255 1.255 0 00-1.225 0L4.011 5.465c-.392.222-.636.64-.636 1.088v.091a9.474 9.474 0 004.824 8.248l.19.106zm1.06-10.381l3.198 1.799a.916.916 0 01.465.796v.066a6.925 6.925 0 01-3.525 6.029l-.138.077c-.225.127-.448-.093-.448-.351V4.967c0-.258.222-.477.447-.35z" fill="#969493"></path></svg>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M8.388 14.998a1.249 1.249 0 001.224 0l.19-.106a9.474 9.474 0 004.823-8.248v-.09c0-.45-.244-.867-.637-1.09L9.611 3.003a1.255 1.255 0 00-1.225 0L4.011 5.465c-.392.222-.636.64-.636 1.088v.091a9.474 9.474 0 004.824 8.248l.19.106zm1.06-10.381l3.198 1.799a.916.916 0 01.465.796v.066a6.925 6.925 0 01-3.525 6.029l-.138.077c-.225.127-.448-.093-.448-.351V4.967c0-.258.222-.477.447-.35z" fill="#969493"></path></svg>
                      </div>
                      <p className='more-text'>Вернём до 50000 ₽ при поломке или утере</p>
                      <div style={{ width: 24 }} className='icon-container'>
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M13.52 12.435l-1.29-.58V5.39l1.29.58v6.465zM7.064 5.39l1.291.58v6.465l-1.291-.58V5.39zm7.369-.426l-2.584-1.162-.03-.009a.717.717 0 00-.317-.04c-.024.003-.046.01-.07.015a.788.788 0 00-.083.025c-.01.004-.02.005-.03.01L9 4.843 6.681 3.802l-.03-.009a.714.714 0 00-.316-.04c-.024.003-.047.01-.07.015a.784.784 0 00-.084.025c-.01.004-.02.005-.03.01L3.57 4.962a.645.645 0 00-.381.59v7.88a.647.647 0 00.91.589l2.319-1.042 2.318 1.042c.01.005.02.003.03.006a.63.63 0 00.235.05.63.63 0 00.234-.05c.01-.003.022-.002.03-.006l2.32-1.042 2.318 1.042a.645.645 0 00.91-.589v-7.88a.645.645 0 00-.38-.59z" fill="#969493"></path></svg>
                      </div>
                      <p className='more-text'>Работаем по городу и области</p>
                    </>
                  }
                </div>
              </div>
              <div className='more' onClick={()=> setMore(state => !state)}>
                Подробнее
                <svg width="24" height="24" fill="hsl(339, 100%, 62%)" xmlns="http://www.w3.org/2000/svg"><path clipRule="evenodd" d="M16 14.5a.997.997 0 01-.707-.293l-3.305-3.305-3.293 3.18a1 1 0 01-1.39-1.439l4-3.862a1 1 0 011.402.012l4 4A.999.999 0 0116 14.5z"></path></svg>
              </div>
            </Card.Body>
          </Card>

          <div className='mt-5 ml-144 mr-24'>
            <p className='fw-bold fs-6 mb-2'>Как доставить</p>
            <div className='d-flex gap-2'>
                <div className='card-sm' style={{ borderColor: selectTransport === 'on-foot' ? '#ff3d81' : '#ddd' }} onClick={() => handleChangeFoot()}>Пешком</div>
                <div className='card-sm' style={{ borderColor: selectTransport === 'car' ? '#ff3d81' : '#ddd' }} onClick={() => handleChangeCar()}>Легковой автомобиль</div>
                <div className='card-sm' style={{ borderColor: selectTransport === 'lorry' ? '#ff3d81' : '#ddd' }} onClick={() => setSelectTransport('lorry')}>Грузовой</div>
            </div>
            { selectTransport === 'on-foot' ? 
              <div>
                <p className='fw-bold fs-6 mb-2 mt-4'>Вес посылки</p>
                <div className='d-flex gap-2'>
                    <div className='card-sm' style={{ borderColor: selectWeight === 1 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(1)}>до 1 кг</div>
                    <div className='card-sm' style={{ borderColor: selectWeight === 5 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(5)}>до 5 кг</div>
                    <div className='card-sm' style={{ borderColor: selectWeight === 10 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(10)}>до 10 кг</div>
                    <div className='card-sm' style={{ borderColor: selectWeight === 15 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(15)}>до 15 кг</div>
                </div>
              </div> : selectTransport === 'car' ? 
                <div>
                  <p className='fw-bold fs-6 mb-2 mt-4'>Вес посылки</p>
                  <div className='d-flex gap-2'>
                      <div className='card-sm' style={{ borderColor: selectWeight === 50 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(50)}>до 50 кг</div>
                      <div className='card-sm' style={{ borderColor: selectWeight === 100 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(100)}>до 100 кг</div>
                      <div className='card-sm' style={{ borderColor: selectWeight === 150 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(150)}>до 150 кг</div>
                      <div className='card-sm' style={{ borderColor: selectWeight === 200 ? '#ff3d81' : '#ddd' }} onClick={() => setSelectWeight(200)}>до 200 кг</div>
                  </div>
                </div> : 
                  <div>
                    <Row className='mt-4'>
                      <Col md={3}>
                        <div className='card-xs d-flex align-items-center gap-2' style={{ borderColor: selectCar === 'jip' ? '#ff3d81' : '#ddd' }} onClick={() => setSelectCar('jip')}>
                          <Image src='https://dostavista.ru/img/gv/icon-pickup.png' />
                          <div>
                            <p className='mb-0 car-text'>Джип/Пикап</p>
                            <p className='mb-0 car-text'>до 2 м3, 500 кг</p>
                          </div> 
                        </div>
                      </Col>
                      <Col md={3}>
                        <div className='card-xs d-flex align-items-center gap-2' style={{ borderColor: selectCar === 'kabluk' ? '#ff3d81' : '#ddd' }} onClick={() => setSelectCar('kabluk')}>
                          <Image src='https://dostavista.ru/img/gv/icon-kabluk.png' />
                          <div>
                            <p className='mb-0 car-text'>Каблук</p>
                            <p className='mb-0 car-text'>до 3,2 м3, 700 кг</p>
                          </div> 
                        </div>
                      </Col>
                      <Col md={3}>
                        <div className='card-xs d-flex align-items-center gap-2' style={{ borderColor: selectCar === 'porter' ? '#ff3d81' : '#ddd' }} onClick={() => setSelectCar('porter')}>
                          <Image src='https://dostavista.ru/img/gv/icon-porter.png' />
                          <div>
                            <p className='mb-0 car-text'>Портер</p>
                            <p className='mb-0 car-text'>до 8 м3, 1000 кг</p>
                          </div> 
                        </div>
                      </Col>
                      <Col md={3}>
                        <div className='card-xs d-flex align-items-center gap-2' style={{ borderColor: selectCar === 'gazel' ? '#ff3d81' : '#ddd' }} onClick={() => setSelectCar('gazel')}>
                          <Image src='https://dostavista.ru/img/gv/icon-gazel.png' />
                          <div>
                            <p className='mb-0 car-text'>Газель</p>
                            <p className='mb-0 car-text'>до 17 м3, 1500 кг</p>
                          </div> 
                        </div>
                      </Col>
                    </Row>
                    
                    <p className='fw-bold fs-6 mb-2 mt-4'>Вес посылки</p>
                    <Form.Control type="text" value='0' placeholder="Normal text" className='w-25' />
                  </div>
            }
          </div>

          <div className='map'>
            <div className='d-flex gap-3 mt-5 mx-4 map-card-wrapper'>
              <div>
                <span className='fs-6 from'>Откуда</span>
                <span className='num'>1</span>
                <div className='points'></div>
              </div>
              <Card className='map-card shadow-sm'>
                <Card.Body>
                  <InputGroup className="mb-3">
                    <InputGroup.Text id="basic-addon1" className='p-0 cursor-pointer' onClick={() => setShow(true)}>
                      <Image src='https://dostavista.ru/images/map_c8b6a3b6d31d298173c5007c0a691d51.svg' />
                    </InputGroup.Text>
                    <Form.Control
                      placeholder="Amir Temur, 7"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                    />
                  </InputGroup>
                
                <div className='d-flex justify-content-between'>
                  <MaskedInput
                    value={value}
                    mask={phoneValidation.mask}
                    guide={true}
                    showMask={true}
                    keepCharPositions={true}
                    placeholder={phoneValidation.placeholder}
                    placeholderChar={'_'}
                    style={{ width: 160 }}
                    onChange={v => setValue(v)}
                  />
                  
                  { selectCard === 'first' ? <div className='d-flex gap-2 align-items-center'>
                      <svg width="24" height="24" fill="hsl(339, 100%, 62%)" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M19.875 5.25a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0zm-7.02 1.18a1.125 1.125 0 00-.97.132L9.128 8.384a1.125 1.125 0 101.242 1.877l2.3-1.523 1.066.35-2.034 3.151c-.02.032-.04.065-.057.098l-1.273 2.492-2.624-.596a1.125 1.125 0 00-.498 2.194l3.477.79a1.125 1.125 0 001.251-.585l1.049-2.053.685.54-1.108 4.395a1.125 1.125 0 002.131.722l1.364-5.148c.153-.452.005-.95-.37-1.245l-1.581-1.244 1.509-2.337.793 1.225c.208.32.563.513.944.513h2.48a1.125 1.125 0 000-2.25h-1.868l-1.125-1.737a1.125 1.125 0 00-.594-.457L12.855 6.43zM5.25 10.876a1.125 1.125 0 100 2.25h1.687a1.125 1.125 0 100-2.25H5.25zM3 8.625C3 8.004 3.504 7.5 4.125 7.5h1.687a1.125 1.125 0 110 2.25H4.125A1.125 1.125 0 013 8.625zm3.375-4.5a1.125 1.125 0 100 2.25h1.687a1.125 1.125 0 100-2.25H6.375z"></path></svg>
                      <p className='fs-6 mb-0'>{ couirer ? 'Курьер будет с 11:31 до 12:31' : 'Введите адрес, чтобы узнать, когда приедет курьер'}</p>
                    </div> : <div className='d-flex align-items-center gap-3'>
                        <SelectPicker searchable={false} data={data} />
                          <span>с</span>
                        <SelectPicker searchable={false} data={data} />
                          <span>до</span>
                        <SelectPicker searchable={false} data={data} />
                    </div>
                  }
                </div>

                { !more2 ? <p className='more mb-0' onClick={() => setMore2(true)}>Дополнительно</p> 
                  : <Row className='mt-3'>
                    <Col md={3}>
                      <Input placeholder='Контактное лицо'/>
                    </Col>
                    <Col md={3}>
                      <Input placeholder='Внутр. номер заказа'/>
                    </Col>
                    <Col md={6}>
                      <SelectPicker style={{ width: '100%' }} />
                    </Col>
                    <Col md={12} className='close mt-2' onClick={() => setMore2(false)}>Скрыть дополнительные поля</Col>
                  </Row>
                }
                </Card.Body>
              </Card>
            </div>
            { destination.map((d, i) => (
              <div key={i} className='d-flex gap-3 mt-4 mx-4 map-card-wrapper'>
                <div className='d-flex justify-content-end' style={{ width: 95 }}>
                  <span className='fs-6 from'>Куда</span>
                  <span className='num'>{i+2}</span>
                  <div className='points' style={d.pointStyle}></div>
                </div>
                <Card className='map-card shadow-sm'>
                  <Card.Body style={d.cardStyle}>
                    <InputGroup className="mb-3">
                      <InputGroup.Text id="basic-addon1" className='p-0 cursor-pointer' onClick={() => setShow(true)}>
                        <Image src='https://dostavista.ru/images/map_c8b6a3b6d31d298173c5007c0a691d51.svg' />
                      </InputGroup.Text>
                      <Form.Control
                        placeholder="Navoiy, 7"
                        aria-label="Username"
                        aria-describedby="basic-addon1"
                      />
                    </InputGroup>

                    <div className='d-flex justify-content-between'>
                      <MaskedInput
                        // value={value}
                        mask={phoneValidation.mask}
                        guide={true}
                        showMask={true}
                        keepCharPositions={true}
                        placeholder={phoneValidation.placeholder}
                        placeholderChar={'_'}
                        style={{ width: 160 }}
                        // onChange={v => setValue(v)}
                      />
                      
                      { selectCard === 'second' && <div className='d-flex align-items-center gap-3'>
                          <SelectPicker searchable={false} data={data} />
                            <span>с</span>
                          <SelectPicker searchable={false} data={data} />
                            <span>до</span>
                          <SelectPicker searchable={false} data={data} />
                        </div>
                      }
                    </div>

                    { !d.more ? <p className='more mb-0' onClick={() => handleMore(i)}>Дополнительно</p> 
                      : <Row className='mt-3'>
                        <Col md={3}>
                          <Input placeholder='Контактное лицо'/>
                        </Col>
                        <Col md={3}>
                          <Input placeholder='Внутр. номер заказа'/>
                        </Col>
                        <Col md={6}>
                          <SelectPicker style={{ width: '100%' }} />
                        </Col>
                        <Col md={12} className='close mt-2' onClick={() => handleCloseMore(i)}>Скрыть дополнительные поля</Col>
                      </Row>
                    }
                    <button type="button" className="btn-close" style={d.btnStyle} onClick={() => handleRemoveDestination(i)}></button>
                  </Card.Body>
                </Card>
              </div>
            ))}
            <button className='btn-contained ml-144 mt-3' onClick={handleAddDestination}>Добавить адрес</button>
          </div>

          <Card className='order-type-card mt-5 ml-144 mr-24 shadow-sm'>
            <Card.Body>
              <p className='fw-bold fs-6 mb-3'>Что везём</p>
              <Form.Control ref={focusInput} type="text" placeholder='Что везём' value={orderType} onChange={(e) => setOrderType(e.target.value)} />
              <div>
                <span onClick={()=> setOrderType('Документы')}>Документы</span>
                <span onClick={()=> setOrderType('Продукты')}>Продукты</span>
                <span onClick={()=> setOrderType('Горячая еда')}>Горячая еда</span>
                <span onClick={()=> setOrderType('Цветы')}>Цветы</span>
                <span onClick={()=> setOrderType('Торт')}>Торт</span>
                <span onClick={()=> setOrderType('Подарок')}>Подарок</span>
                <span onClick={()=> handleOther()}>Другое</span>
              </div>
              { selectTransport !== 'lorry' ? 
                <div className='d-flex mt-4 gap-2'>
                  <Form.Check 
                    type='checkbox'
                    id='default-checkbox'
                    label='Посылка большого размера'
                  />
                  <Whisper 
                    placement="top" 
                    trigger="hover" 
                    controlId="control-id-hover" 
                    speaker={
                      <Popover style={{ width: 320, borderRadius: 16, padding: 20 }}>
                        <p style={{ fontSize: 14, fontWeight: 700, color: '#2d2928', marginBottom: 0}}>Посылка большого размера</p>
                        { selectTransport === 'on-foot' ?
                          <p style={{ fontSize: 14, color: '#2d2928', marginBottom: 0}}>Такая посылка не поместится в городской рюкзак.</p>
                          : <p style={{ fontSize: 14, color: '#2d2928', marginBottom: 0}}>Такая посылка не поместится в багажник автомобиля.</p>
                        }
                      </Popover>
                    }
                  >
                    <img src='https://dostavista.ru/images/question_1e966b2db1cf70e858339aad6bf0764e.svg' />
                  </Whisper>
                </div> : <Form.Control className='w-50 mt-4' type="text" placeholder='Габариты' />
              }
              
              
            </Card.Body>
          </Card>

          <Card className='package-value-card mt-5 ml-144 mr-24 shadow-sm'>
            <Card.Body>
              <div className='fw-bold fs-6 mb-3'>Ценность посылки</div>
              <Form.Control className='w-25 d-inline-block me-3' type="text" placeholder='Ценность посылки' value={packageValue} onChange={handlePackageValue} />
              <span>плюс {(packageValue*0.005).toFixed(0)} ₽ к заказу</span>
              <p className='mt-4'>Компенсируем ценность утерянных отправлений и выручки в течение трех рабочих дней по <a href='#'>регламенту</a>. Максимальная компенсация — 50 000₽.</p>
            </Card.Body>
          </Card>

          <div className='mt-5 ml-144 mr-24'>
            <div className='fw-bold fs-6 mb-3'>Способ оплаты</div>
            <div className='d-flex gap-3'>
              <div className='card-normal' style={{ borderColor: paymentMethod === 'cash' ? '#ff3d81' : '#ddd' }} onClick={() => setPaymentMethod('cash')}>
                <Image src='https://dostavista.ru/images/cash-icon_9ad59cc3d01ddd97b7ca93a8eb8b14ed.svg' />
                <div>
                  <p className='mb-1'>Наличными курьеру</p>
                  <p className='desc'>Выберите адрес оплаты после отправки заказа</p>
                </div>
              </div>
              <div className='card-normal' style={{ borderColor: paymentMethod === 'card' ? '#ff3d81' : '#ddd' }} onClick={() => setPaymentMethod('card')}>
                <Image src='https://dostavista.ru/images/card-icon_a935a2f9d7f51d8126a22c8ca5b582c0.svg' />
                <div>
                  <p className='mb-1'>Картой онлайн</p>
                  <p className='desc'>Visa, MasterСard, МИР и другие</p>
                </div>
              </div>
            </div>

            { !promokod ? <p className='promokod mt-4' onClick={() => setPromokod(true)}>У меня есть промокод</p> 
              : <div className='mt-4'>
                <Form.Control style={{ width: 290 }} className='d-inline-block me-3' type="text" placeholder='Промокод' />
                <button className='btn-contained'>Применить</button>
              </div>
            }
          </div>

          <Card ref={totalCard} className='total-card mt-5 ml-144 mr-24 shadow-sm'>
            <Card.Body>
              <div className='fs-2 my-3'>Итого: от {summa} ₽</div>
              <button style={{ width: 256, paddingTop: 11, paddingBottom: 11 }} className='btn-contained mt-3'>Отправить заказ</button>
              <p className='pt-3 w-75'>Параметры смс-уведомлений для вас и получателей можно настроить после подтверждения заказа.</p>
              <br />
              <br />
              <p>Нажимая кнопку «Отправить заказ», вы соглашаетесь с <a href='#'>Правилами использования ПО Dostavista</a> и <a href='#'>Политикой по обработке персональных данных</a>.</p>
            </Card.Body>
          </Card>
          
          
          <Modal show={show} fullscreen={true} onHide={() => handleModal}>
            <Modal.Header>
              <Modal.Title>Modal</Modal.Title>
              <button type="button" class="btn-close" aria-label="Close" onClick={handleModal}></button>
            </Modal.Header>
            <YMaps>
              <Map width='100%' height='100%' state={mapState}>
                <Placemark geometry={currentCity} />
              </Map>
            </YMaps>
          </Modal>

          { height && 
            <div className='fixed-total'>
              Итого: от {summa} ₽
            </div>
          }
          
        </div>
      </div>
    </div>
  );
}

export default App;
